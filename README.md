# nginx编译安装依赖包

## 概览

本仓库专门提供在CentOS 7.9系统上编译安装Nginx所需的各种依赖包。当您希望从源代码编译安装Nginx，以获得更高的定制性或因为特定版本的需求时，这些依赖是必不可少的。通过直接利用本仓库中的资源，您可以避免手动查找和逐一下载每个依赖包的繁琐过程，确保编译环境的一致性和兼容性。

## 包含依赖

此资源集合包含了但不限于以下依赖项：
- PCRE (Perl Compatible Regular Expressions) - 提供正则表达式支持
- OpenSSL - 实现HTTPS安全连接所必需的安全库
- zlib - 支持HTTP压缩功能
- pcre-devel, zlib-devel - 编译时需要的开发库文件，用于链接和编译Nginx中的相关特性
- OpenLDAP (可选) - 如果Nginx配置需要使用OpenLDAP进行身份验证
- ngx_http_ssl_module相关依赖 - 确保SSL模块的支持

请注意，具体包含的依赖包列表可能会根据Nginx的版本需求有所调整。

## 使用说明

1. **克隆仓库**  
   使用Git命令克隆本仓库到您的服务器或本地环境中。
   ```shell
   git clone https://github.com/your-repo-url.git
   ```

2. **切换至依赖包目录**  
   进入刚刚克隆的仓库目录。
   ```shell
   cd nginx-dependencies
   ```

3. **安装依赖**  
   根据您的实际需求，使用`yum install`或`dnf install`命令安装解压后的依赖包（如果提供了RPM包或者需要手动编译的源码包请参照附带的说明文档）。
   
4. **编译安装Nginx**  
   在完成依赖安装后，下载Nginx的源码包，并遵循Nginx官方文档进行编译安装。通常步骤包括配置、编译和安装。
   ```shell
   # 下载Nginx源码包，示例使用1.19.10版本
   wget http://nginx.org/download/nginx-1.19.10.tar.gz
   tar -xzvf nginx-1.19.10.tar.gz
   cd nginx-1.19.10
   ./configure --with-http_ssl_module # 及其它可能需要的配置选项
   make && sudo make install
   ```

5. **启动Nginx**  
   安装完成后，启动Nginx服务。
   ```shell
   sudo systemctl start nginx
   ```

## 注意事项

- 请根据您的Nginx版本和具体需求调整配置选项。
- 某些依赖包可能已有预编译的RPM或DEB包，但在追求最适配的环境下，从源码编译可能更为推荐。
- 确保操作系统已更新至最新状态，以获取最佳兼容性和安全性。

通过这个仓库，我们旨在简化Nginx的编译安装流程，帮助开发者和运维人员高效地搭建起自己的Web服务器环境。如果您在使用过程中遇到任何问题，欢迎提交issue或参与讨论。